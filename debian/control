Source: libminini
Section: libs
Priority: optional
Maintainer: Yangfl <mmyangfl@gmail.com>
Build-Depends: dpkg-dev (>= 1.22.5),
 debhelper-compat (= 13),
Rules-Requires-Root: no
Standards-Version: 4.7.2
Homepage: https://www.compuphase.com/minini.htm
Vcs-Git: https://salsa.debian.org/yangfl-guest/minIni.git
Vcs-Browser: https://salsa.debian.org/yangfl-guest/minIni

Package: libminini-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libminini1t64 (= ${binary:Version}), ${misc:Depends}
Description: minimal INI file parser - development headers
 minIni is a programmer's library to read and write "INI" files in embedded
 systems. minIni takes little resources, has a deterministic memory footprint
 and can be configured for various kinds of file I/O libraries. The principal
 purpose for minIni is to be used on embedded systems that run on an RTOS (or
 even without any operating system). minIni requires that such a system provides
 a kind of storage and file I/O system, but it does not require that this file
 I/O system is compatible with the standard C/C++ library.
 .
 This package contains the development headers.

Package: libminini1t64
Provides: ${t64:Provides}
Replaces: libminini1
Breaks: libminini1 (<< ${source:Version})
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: minimal INI file parser
 minIni is a programmer's library to read and write "INI" files in embedded
 systems. minIni takes little resources, has a deterministic memory footprint
 and can be configured for various kinds of file I/O libraries. The principal
 purpose for minIni is to be used on embedded systems that run on an RTOS (or
 even without any operating system). minIni requires that such a system provides
 a kind of storage and file I/O system, but it does not require that this file
 I/O system is compatible with the standard C/C++ library.
