#!/bin/sh
set -e

cd tests
cc -o test1 test.c -lminIni
c++ -o test2 test2.cc -lminIni
./test1
./test2
rm -f test1 test2
