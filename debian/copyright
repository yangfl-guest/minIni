Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: minIni
Source: https://www.compuphase.com/minini.htm
Files-Excluded: minIni.pdf

Files: *
Copyright: 1994 Joseph J. Graf
           2008-2017 CompuPhase
           Steven Van Ingelgem
           Luca Bassanello
License: Apache-2 with link exception

Files: minGlue*.h
Copyright: 2008-2017 CompuPhase
           Luca Bassanello
License: public-domain
 This "glue file" is in the public domain. It is distributed without
 warranties or conditions of any kind, either express or implied.

Files: debian/*
Copyright: 2018-2025 Yangfl <mmyangfl@gmail.com>
License: Apache-2 with link exception

License: Apache-2 with link exception
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 https://www.apache.org/licenses/LICENSE-2.0
 .
 As a special exception to the Apache License 2.0 (and referring to the
 definitions in Section 1 of that license), you may link, statically or
 dynamically, the "Work" to other modules to produce an executable file
 containing portions of the "Work", and distribute that executable file in
 "Object" form under the terms of your choice, without any of the additional
 requirements listed in Section 4 of the Apache License 2.0. This exception
 applies only to redistributions in "Object" form (not "Source" form) and only
 if no modifications have been made to the "Work".
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache version 2.0 license
 can be found in "/usr/share/common-licenses/Apache-2.0".
